//
//  ViewController.swift
//  coffee_raphael
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
struct Cafe : Codable {
    let file : String
 
}

class ViewController: UIViewController {

    @IBOutlet weak var Image: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    func downloadcCafe () {
        guard let url = URL(string:"https://coffee.alexflipnote.dev/random.json") else  {
            return
        }
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let data = data {
                let cafe = try! JSONDecoder().decode(Cafe.self, from : data)
                self.downloadimage(url : cafe.file)
            
            }
        }.resume()
    }
    
    @IBAction func button(_ sender: Any) {
       
        downloadcCafe()
    }
    

    func downloadimage (url: String) {
        guard let imageUrl = URL(string: url) else {
            return
        }
        URLSession.shared.dataTask(with: imageUrl) {(data, response, error) in
            if let data = data{
                let image = UIImage(data: data)
                DispatchQueue.main.async {
                    self.Image.image = image
                   
                }
               
            }
        }.resume()
        
    }
    
}



